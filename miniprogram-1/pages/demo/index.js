// pages/demo/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list:[
    {name:"首页",id:1,isCheck:true},
    {name:"详情",id:1,isCheck:false},
    {name:"购物车",id:1,isCheck:false},
    {name:"商城",id:1,isCheck:false},
    {name:"我的",id:1,isCheck:false},
    ]
  },
  handletap(e){
    // console.log(e.detail.index)
    let index = e.detail.index
    let list =  this.data.list
    // foreach遍历数组 判断数组索引和接收的索引是否相等 相等代表选中 将isCheck改为true 其余皆为false
    list.forEach((v,i) => i===index?v.isCheck=true : v.isCheck=false
    );
    // 改变过后的数组赋值回data中
    this.setData({
      list
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})