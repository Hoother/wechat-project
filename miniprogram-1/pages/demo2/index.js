// pages/demo2/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    number:0,
    num:1,
    minusStatus:'disabled'

  },
handleinput(e){
  // console.log(e)
  let add = e.currentTarget.dataset.add
  this.setData({
    number:this.data.number + add
  })
},
bindMinus: function() {  
  var num = this.data.num;  
  // 如果大于1时，才可以减  
  if (num > 1) {  
      num --;  
  }  
  // 只有大于一件的时候，才能normal状态，否则disable状态  
  var minusStatus = num <= 1 ? 'disabled' : 'normal';  
  // 将数值与状态写回  
  this.setData({  
      num: num,  
      minusStatus: minusStatus  
  });  
},  
/* 点击加号 */  
bindPlus: function() {  
  var num = this.data.num;  
  // 不作过多考虑自增1  
  num ++;  
  // 只有大于一件的时候，才能normal状态，否则disable状态  
  var minusStatus = num < 1 ? 'disabled' : 'normal';  
  // 将数值与状态写回  
  this.setData({  
      num: num,  
      minusStatus: minusStatus  
  });  
},  
/* 输入框事件 */  
bindManual: function(e) {  
  var num = e.detail.value;  
  // 将数值与状态写回  
  this.setData({  
      num: num  
  });  
}  
}) 