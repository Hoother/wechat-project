// Components/tabs/zujian.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    list: {
      type: Array,
      value: [],
    },
    str: {
      type: String,
      value: "",
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    // list:[
    //   // {name:"首页",id:1,isCheck:true},
    //   // {name:"详情",id:1,isCheck:true},
    //   // {name:"购物车",id:1,isCheck:true},
    //   // {name:"我的",id:1,isCheck:true},
    //   // {name:"商城",id:1,isCheck:true},
    // ]
  },
  // handletap(e) {
  //   let {
  //     index
  //   } = e.currentTarget.dataset
  //   this.triggerEvent('handleindex', {
  //     index
  //   })
  // },
  /**
   * 组件的方法列表
   */
  methods: {
    handletap(e) {
      // 结构赋值 接收索引
      let { index } = e.currentTarget.dataset;
      // 获取data中的list数组
      let list = this.data.list;
      this.triggerEvent("handleindex", {
        index,
      });
      // console.log(e.currentTarget.dataset.index);
      // foreach遍历数组 判断数组索引和接收索引是否相等 相等代表选中 将isCheck该为true 其余皆为
      list.forEach((v, i) =>
        i === index ? (v.isCheck = true) : (v.isCheck = false)
      );
      //将改变过后的数组赋值回data中
      this.setData({
        list,
      });
      console.log(list);
    },
  },
});
